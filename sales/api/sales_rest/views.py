from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, SalesPerson, Customer, SaleRecord
from .encoders import (
    SalesPersonEncoder,
    CustomerEncoder,
    SaleRecordEncoder,
    AutomobileVOEncoder
)


@require_http_methods(["GET"])
def list_automobileVOs(request):
    automobiles = AutomobileVO.objects.all()
    return JsonResponse({"autos": automobiles}, encoder=AutomobileVOEncoder)


@require_http_methods(["GET"])
def list_unsold_automobileVOs(request):
    automobiles = AutomobileVO.objects.all()
    unsold_automobiles = []
    for automobile in automobiles:
        try:
            SaleRecord.objects.get(automobile__vin=automobile.vin)
        except SaleRecord.DoesNotExist:
            unsold_automobiles.append(automobile)

    return JsonResponse({"unsold_autos": unsold_automobiles}, encoder=AutomobileVOEncoder)


@require_http_methods(["GET", "POST"])
def list_sales_people(request):
    if request.method == 'GET':
        sales_people = SalesPerson.objects.all()
        return JsonResponse({"sales_people": sales_people}, encoder=SalesPersonEncoder)
    else:
        content = json.loads(request.body)

        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def show_sales_person(request, pk):
    if request.method == 'GET':
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "ID is not valid"}, status=404)

    elif request.method == 'PUT':
        try:
            content = json.loads(request.body)
            SalesPerson.objects.filter(id=pk).update(**content)
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "ID is not valid"}, status=404)
    else:
        try:
            count, _ = SalesPerson.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "ID is not valid"}, status=404)


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def show_customer(request, pk):
    if request.method == 'GET':
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "ID is not valid"}, status=404)
    elif request.method == 'PUT':
        try:
            content = json.loads(request.body)
            Customer.objects.filter(id=pk).update(**content)
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "ID is not valid"}, status=404)
    else:
        try:
            count, _ = Customer.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Customer.DoesNotExist:
            return JsonResponse({"message": "ID is not valid"}, status=404)


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == 'GET':
        sales = SaleRecord.objects.all()
        return JsonResponse({"sales": sales}, encoder=SaleRecordEncoder)
    else:
        content = json.loads(request.body)

        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile href"},
                status=400,
            )

        try:
            sales_person_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=sales_person_id)
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sales_person id"},
                status=400,
            )

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )


        sales_record = SaleRecord.objects.create(**content)
        return JsonResponse(
            sales_record,
            encoder=SaleRecordEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def show_sale(request, pk):
    if request.method == 'GET':
        try:
            sale = SaleRecord.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SaleRecordEncoder,
                safe=False
            )
        except SaleRecord.DoesNotExist:
            return JsonResponse({"message": "ID is not valid"}, status=404)
    elif request.method == 'PUT':
        try:

            content = json.loads(request.body)

            try:
                if "automobile" in content:
                    automobile = AutomobileVO.objects.get(import_href=content["automobile"])
                    content["automobile"] = automobile
            except AutomobileVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid automobile href"},
                    status=400,
                )

            try:
                sales_person_id = content["sales_person"]
                sales_person = SalesPerson.objects.get(id=sales_person_id)
                content["sales_person"] = sales_person
            except SalesPerson.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid sales_person id"},
                    status=400,
                )

            try:
                customer_id = content["customer"]
                customer = Customer.objects.get(id=customer_id)
                content["customer"] = customer
            except Customer.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid customer id"},
                    status=400,
                )

            SaleRecord.objects.filter(id=pk).update(**content)
            sale = SaleRecord.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SaleRecordEncoder,
                safe=False,
            )
        except SaleRecord.DoesNotExist:
            return JsonResponse({"message": "ID is not valid"}, status=404)
    else:
        try:
            count, _ = SaleRecord.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except SaleRecord.DoesNotExist:
            return JsonResponse({"message": "ID is not valid"}, status=404)
