from django.urls import path
from .views import list_sales, show_sale, list_sales_people, show_sales_person, list_customers, show_customer, list_automobileVOs, list_unsold_automobileVOs

urlpatterns = [
    path("sales-records/", list_sales, name="list_sales"),
    path("sales-records/<int:pk>/", show_sale, name="show_sale"),
    path("sales-people/", list_sales_people, name="list_sales_people"),
    path("sales-person/<int:pk>/", show_sales_person, name="show_sales_person"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:pk>/", show_customer, name="show_customer"),
    path("autos/", list_automobileVOs, name="list_autos"),
    path("unsold-autos/", list_unsold_automobileVOs, name="list_unsold_autos"),
]
